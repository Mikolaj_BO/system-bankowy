/**
 * Created by Mikołaj on 11.04.2017.
 */

public class NotFoundID extends Exception {

    NotFoundID(String message){
        super(message);
    }
}
