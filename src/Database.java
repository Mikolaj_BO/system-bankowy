import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mikołaj on 08.04.2017.
 */

public class Database{

    private Map<Integer,Client> data = new HashMap<Integer,Client>();


    public Database(String filePath){

        readDataFromFile(filePath);
    }



    public void addDataToFile(String filePath){

        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(filePath))) {

            outputStream.writeObject(data);

        }catch(IOException e){

            System.out.println(e.getMessage());
        }

    }

    public void readDataFromFile(String filePath){

        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(filePath))) {

           this.data = (HashMap) inputStream.readObject();

        }catch(ClassNotFoundException | IOException e){

            System.out.println(e.getMessage());
        }
    }



    public Map<Integer, Client> getData() {
        return data;
    }

}
