
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;


/**
 * Created by Mikołaj on 08.04.2017.
 */
public class Client implements Serializable {

    private int clientID;
    private String firstName;
    private String lastName;
    private String pesel;
    private String adress ;
    private double accountBalance;

    public Client(){

        clientID = 0;
        firstName = null;
        lastName = null;
        pesel = null;
        adress = null;
        accountBalance = 0.0;
    }

    public Client(Map<Integer,Client> clients){

        int i = 0;
        do{
            i++;
            clientID = i;
        }while(clients.containsKey(i));

        int counter = 0 ;
        Scanner input  = new Scanner(System.in);
        String allData = input.nextLine();

        while((allData.split("\\-", -1).length - 1) != 4){

            System.out.println("Zle podales argumenty\nPodaj jeszcze raz: ");
            allData = input.nextLine();
        }


        Scanner inputL = new Scanner(allData);
        inputL.useDelimiter("-");

        while(inputL.hasNext()){

            counter++;
            if (counter == 1) {
                firstName = inputL.next();

                while (!(firstName.matches("[a-zA-Z]*")) || firstName.isEmpty()) {
                    System.out.println("Imie moze zawierac tylko litery.");
                    System.out.print("Podaj jeszcze raz: ");
                    firstName = input.nextLine();
                }

            } else if (counter == 2) {
                lastName = inputL.next();

                while (!(lastName.matches("[a-zA-Z]*")) || lastName.isEmpty()) {
                    System.out.println("Nazwisko moze zawierac tylko litery.");
                    System.out.print("Podaj jeszcze raz: ");
                    lastName = input.nextLine();
                }

            } else if (counter == 3) {
                pesel = inputL.next();

                while (!(pesel.matches("[0-9]*")) || pesel.length() != 11 || pesel.isEmpty()) {
                    System.out.println("Pesel musi zawierac wylacznie 11 cyfr.");
                    System.out.print("Podaj jeszcze raz: ");
                    pesel = input.nextLine();
                }

            } else if (counter == 4) {
                adress = inputL.next();

                while (!(adress.matches("[0-9a-zA-z]*")) || adress.isEmpty()) {
                    System.out.println("Wykryto niedozwolony znak");
                    System.out.print("Podaj jeszcze raz: ");
                    adress = input.nextLine();
                }

            } else if (counter == 5) {

                boolean checkNum = false;
                String temp = inputL.next();

                while (!checkNum) {

                    checkNum = true;
                    try {
                        accountBalance = Double.parseDouble(temp);
                    } catch (NumberFormatException e) {

                        System.out.println("Niepoprawnie podana stan konta, miejsca dziesietne nalezy oddzielac:[ . ]");
                        checkNum = false;
                    }

                    if (BigDecimal.valueOf(accountBalance).scale() > 2) {
                        System.out.println("Podales nieporawidlowa liczbe miejsc po przecinku dla stanu konta");
                        checkNum = false;
                    }

                    if (accountBalance < 0) {
                        System.out.println("Stan konta nie moze byc mniejszy od zera");
                        checkNum = false;
                    }

                    if(!checkNum) {
                        System.out.println("Podaj liczbe jeszcze raz: ");
                        temp = input.nextLine();
                        checkNum = false;
                    }
                }
            }

        }

    }





    public int getClientID() {
        return clientID;
    }

    public void setClientID(int client) {
        this.clientID = client;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance)throws IllegalMoneyException {
        if(accountBalance < 0) throw new IllegalMoneyException("Nie wystarczajaca ilosc hajsu");
        this.accountBalance = accountBalance;
    }

    @Override
    public String toString() {
        return  firstName + " " +
                lastName + " " +
                pesel + " " +
                adress + " " +
                accountBalance
                ;
    }
}
