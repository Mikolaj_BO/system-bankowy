import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Mikołaj on 10.04.2017.
 */
public class BankOperation {


    public static void addUser(Database clientsBase){

        System.out.println("Podaj wszystkie dane w nastepujacej kolejnosci:");
        System.out.println("[Imie]-[Nazwisko]-[Pesel]-[Adress]-[Stan Konta]\n[-] -Separator");
        Client newClient = new Client(clientsBase.getData());
        if(confirmOp())
        clientsBase.getData().put(newClient.getClientID(),newClient);
        else return;
    }

    public static void removeUser(Database clientsBase){

        int choice = 0;
        System.out.println("Którego uzytkownika chcesz usunac?");
        try {

            choice = findById(clientsBase);

        }catch(NotFoundID e){
            System.out.println(e.getMessage());
            return;
        }

        if(confirmOp())
            clientsBase.getData().remove(choice);
        else return;


    }

    public static void transfer(Database clientsBase){

        int transferFromId = 0,transferToId = 0;
        double moneyFrom,moneyTo,money = 0.0;
        Scanner input = new Scanner(System.in);

        moneyFrom = clientsBase.getData().get(transferFromId).getAccountBalance();
        moneyTo = clientsBase.getData().get(transferFromId).getAccountBalance();

        try {

            System.out.println("Z ktorego konta chcesz dokonac przelewu?");
            transferFromId = findById(clientsBase);

            System.out.println("Na ktore?");
            transferToId = findById(clientsBase);

        }catch(NotFoundID e){
            System.out.println(e.getMessage());
            return;
        }

        try {
            System.out.print("Podaj ilosc pieniędzy:");
            money = Double.parseDouble(input.nextLine());

        }catch(InputMismatchException e){
            System.out.println("Nieprawidlowa liczba zmiennoprzecinkowa");
            input.nextLine();
        }

        if (BigDecimal.valueOf(money).scale() > 2) {
            System.out.println("Podales nieporawidlowa liczbe miejsc po przecinku dla stanu konta");
            return;
        }


        if(confirmOp()) {
            try {
                clientsBase.getData().get(transferFromId).setAccountBalance(moneyFrom - money);
                clientsBase.getData().get(transferToId).setAccountBalance(moneyTo + money);
            }catch(IllegalMoneyException e){
                System.out.println(e.getMessage());
                return;
            }

            System.out.println("Przelales "+money+" na konto: "+clientsBase.getData().get(transferToId).getFirstName());

        }else return;


    }

    public static void payIn_Out(Database clientsBase,String operator){

        Scanner input = new Scanner(System.in);
        double moneyOnAccount;
        double moneyToPayIn_Out = 0.0;
        int accountId = 0;

        try {
            accountId = findById(clientsBase);
        }catch(NotFoundID e){
            System.out.println(e.getMessage());
        }

        moneyOnAccount = clientsBase.getData().get(accountId).getAccountBalance();

        System.out.print("Podaj ilosc pieniędzy: ");

        try {
            moneyToPayIn_Out = Double.parseDouble(input.nextLine());
        }catch(InputMismatchException e){
            System.out.println("Nieprawidlowa liczba zmiennoprzecinkowa");

        }

        if (BigDecimal.valueOf(moneyToPayIn_Out).scale() > 2) {
            System.out.println("Podales nieporawidlowa liczbe miejsc po przecinku dla stanu konta");
            return;
        }



        if(confirmOp()) {
            try {
                if (operator == "+")
                    clientsBase.getData().get(accountId).setAccountBalance(moneyOnAccount + moneyToPayIn_Out);

                else if (operator == "-")
                    clientsBase.getData().get(accountId).setAccountBalance(moneyOnAccount - moneyToPayIn_Out);
            }catch(IllegalMoneyException e){
                System.out.println(e.getMessage());
                return;
            }
        }else return;

    }


    public static void printAll(Database clientsBase){

        if(clientsBase.getData().isEmpty())
            System.out.println("Nie ma zarejestrowanego zadnego klienta!");

        else{
            for(int id: clientsBase.getData().keySet())
                System.out.println(id + ":" + clientsBase.getData().get(id));
        }
    }

    public static void findByCriterion(Database clientsBase){

        Scanner input = new Scanner(System.in);
        boolean check = false;
        String criterion;
        String criterionValue;

        do{
        System.out.println("Podaj kryterium na podstawie którego bedziesz chcial znalezc uzytkownika: ");
        System.out.println("Cryteria:\n-[Imie]\n-[Nazwisko]\n-[Pesel]\n-[Adres]\n-[ID]");

        criterion = input.nextLine();

        if(criterion.equalsIgnoreCase("imie")){
            System.out.print("Podaj jego wartosc: ");
            criterionValue = input.nextLine();

           for(int key : clientsBase.getData().keySet())
               if(clientsBase.getData().get(key).getFirstName().equalsIgnoreCase(criterionValue)) {
                   System.out.println(key + ":" + clientsBase.getData().get(key));
                   check = true;
               }

        }

        else if(criterion.equalsIgnoreCase("nazwisko")){
            System.out.print("Podaj jego wartosc: ");
            criterionValue = input.nextLine();

            for(int key : clientsBase.getData().keySet())
                if(clientsBase.getData().get(key).getLastName().equalsIgnoreCase(criterionValue)) {
                    System.out.println(key + ":" + clientsBase.getData().get(key));
                    check = true;
                }

        }

        else if(criterion.equalsIgnoreCase("pesel")) {
            System.out.print("Podaj jego wartosc: ");
            criterionValue = input.nextLine();

            for (int key : clientsBase.getData().keySet())
                if (clientsBase.getData().get(key).getPesel().equalsIgnoreCase(criterionValue)){
                    System.out.println(key + ":" + clientsBase.getData().get(key));
                    check = true;
                }

        }

        else if(criterion.equalsIgnoreCase("adres")){
            System.out.print("Podaj jego wartosc: ");
            criterionValue = input.nextLine();

            for(int key : clientsBase.getData().keySet())
                if(clientsBase.getData().get(key).getPesel().equalsIgnoreCase(criterionValue)) {
                    System.out.println(key + ":" + clientsBase.getData().get(key));
                    check = true;
                }
        }

        else if(criterion.equalsIgnoreCase("id")){
            System.out.print("Podaj jego wartosc: ");
            criterionValue = input.nextLine();

            for(int key : clientsBase.getData().keySet())
                if(clientsBase.getData().get(key).getClientID() == Integer.parseInt(criterionValue)) {
                    System.out.println(key + ":" + clientsBase.getData().get(key));
                    check = true;
                }
        }
        else{
            System.out.println("Podales nieprawidlowe kryterium");
        }

        if(!check){

            System.out.println("Nie znaleziono uzytkownika o podanym kryterium");
            System.out.print("Chcesz spróbowac jeszcze raz?\n Podaj [true] aby kontynuuowac: ");
            String temp = input.next();
            if(temp.equalsIgnoreCase("true")) check = false;
            else check = true;
            input.nextLine();
        }

        }while(check != true);
    }

    public static void printMenu(){

        System.out.println("========================MENU========================");
        System.out.println("[1]-Dodaj uzytkownika\n[2]-Usun uzytkownika\n[3]-Wyswietl baze\n[4]-Wplac hajs" +
                "[5]-Transfer miedzy kontami\n[6]-Znajdz przez kryterium\n[7]-Wyplac hajs\n[8]-Dodaj zmiany do bazy" +
                "\n[9]-Aby wyjsc bez zapisywania");
        System.out.println("====================================================");
    }

    private static boolean confirmOp(){

        Scanner input = new Scanner(System.in);
        System.out.println("Zatwierdzic operacje?: Y/N");
        String confirm = input.nextLine();

        if(confirm.equalsIgnoreCase("y")) return true;
        else return false;
    }

    private static int findById(Database clientsBase) throws NotFoundID{

        int choice = 0;
        System.out.print("Podaj ID: ");
        Scanner input = new Scanner(System.in);
        try {
            choice = input.nextInt();
            if(!clientsBase.getData().containsKey(choice)) throw new NotFoundID("Nie ma takiego ID");

        }catch(InputMismatchException e){
            System.out.println("Nie podales prawidlowego ID");
            input.nextLine();
        }
        return choice;
    }

}
