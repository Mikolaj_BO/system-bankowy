import java.util.InputMismatchException;

/**
 * Created by Mikołaj on 11.04.2017.
 */
public class IllegalMoneyException extends Exception {
    IllegalMoneyException(String message){

        super(message);
    }

}
