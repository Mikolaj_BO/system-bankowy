import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Mikołaj on 08.04.2017.
 */
public class TestSystem {

    public static void main(String[] args) {

        String filePath = "Database.bin";
        Database clientsBase = new Database(filePath);

        Scanner input = new Scanner(System.in);

        int choice = 0;
        do {

            BankOperation.printMenu();
            try {

                choice = input.nextInt();
            }
            catch(InputMismatchException e){
                System.out.println("Podales nieprawidlowa cyfre z menu");
                input.nextLine();

            }

            switch (choice) {

                case 1:

                    BankOperation.addUser(clientsBase);
                    break;

                case 2:

                    BankOperation.removeUser(clientsBase);
                    break;

                case 3:

                    BankOperation.printAll(clientsBase);
                    break;

                case 4:

                    BankOperation.payIn_Out(clientsBase,"+");
                    break;

                case 5:

                    BankOperation.transfer(clientsBase);
                    break;

                case 6:

                    BankOperation.findByCriterion(clientsBase);
                    break;

                case 7:

                    BankOperation.payIn_Out(clientsBase,"-");
                    break;

                case 8:

                    clientsBase.addDataToFile(filePath);
                    break;

                case 9:
                    System.exit(1);
            }

        }while(choice != 9);
    }
}
