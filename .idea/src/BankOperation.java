import java.util.Scanner;

/**
 * Created by Mikołaj on 10.04.2017.
 */
public class BankOperation {


    public static void addUser(Database clientsBase){

        System.out.println("Podaj wszystkie dane w nastepujacej kolejnosci:");
        System.out.println("[Imie]-[Nazwisko]-[Pesel]-[Adress]-[Stan Konta]\n[-] - Separator");
        Client newClient = new Client(clientsBase.getData());
        if(confirmOp())
        clientsBase.getData().put(newClient.getClientID(),newClient);
        else return;
    }

    public static void removeUser(Database clientsBase){

        int choice;
        System.out.println("Którego uzytkownika chcesz usunac?");
        choice = findById();

        if(confirmOp())
        clientsBase.getData().remove(choice);
        else return;
    }

    public static void transfer(Database clientsBase){

        int transferFromId,transferToId;
        double money,moneyFrom,moneyTo;
        Scanner input = new Scanner(System.in);

        System.out.println("Z ktorego konta chcesz dokonac przelewu?");
        transferFromId = findById();

        System.out.println("Na ktore?");
        transferToId = findById();

        System.out.print("Podaj ilosc pieniędzy:");
        money = input.nextDouble();

        moneyFrom = clientsBase.getData().get(transferFromId).getAccountBalance();
        moneyTo = clientsBase.getData().get(transferFromId).getAccountBalance();

        if(confirmOp()) {
            clientsBase.getData().get(transferFromId).setAccountBalance(moneyFrom - money);
            clientsBase.getData().get(transferToId).setAccountBalance(moneyTo + money);

            System.out.println("Przelales "+money+" na konto: "+clientsBase.getData().get(transferToId).getFirstName());

        }else return;


    }

    public static void payIn_Out(Database clientsBase,String operator){

        Scanner input = new Scanner(System.in);
        double moneyToPayIn_Out,moneyOnAccount;
        int accountId;

        accountId = findById();

        System.out.print("Podaj ilosc pieniędzy: ");
        moneyToPayIn_Out = input.nextDouble();

        moneyOnAccount = clientsBase.getData().get(accountId).getAccountBalance();

        if(confirmOp()) {
            if (operator == "+")
                clientsBase.getData().get(accountId).setAccountBalance(moneyOnAccount + moneyToPayIn_Out);

            else if (operator == "-")
                clientsBase.getData().get(accountId).setAccountBalance(moneyOnAccount - moneyToPayIn_Out);
        }else return;

    }


    public static void printAll(Database clientsBase){

        if(clientsBase.getData().isEmpty())
            System.out.println("Nie ma zarejestrowanego zadnego klienta!");

        else{
            for(int id: clientsBase.getData().keySet())
                System.out.println(id + ":" + clientsBase.getData().get(id));
        }
    }

    public static void findByCriterion(Database clientsBase){

        Scanner input = new Scanner(System.in);
        boolean check = false;
        String criterion;
        String criterionValue;

        do{
        System.out.println("Podaj kryterium na podstawie którego bedziesz chcial znalezc uzytkownika: ");
        System.out.println("Cryteria:\n-[Imie]\n-[Nazwisko]\n-[Pesel]\n-[Adres]\n-[ID]");


        criterion = input.nextLine();
        System.out.print("Podaj jego wartosc: ");
        criterionValue = input.nextLine();

        if(criterion.equalsIgnoreCase("imie")){

           for(int key : clientsBase.getData().keySet())
               if(clientsBase.getData().get(key).getFirstName().equalsIgnoreCase(criterionValue)) {
                   System.out.println(key + ":" + clientsBase.getData().get(key));
                   check = true;
               }

        }

        else if(criterion.equalsIgnoreCase("nazwisko")){

            for(int key : clientsBase.getData().keySet())
                if(clientsBase.getData().get(key).getLastName().equalsIgnoreCase(criterionValue)) {
                    System.out.println(key + ":" + clientsBase.getData().get(key));
                    check = true;
                }

        }

        else if(criterion.equalsIgnoreCase("pesel")) {

            for (int key : clientsBase.getData().keySet())
                if (clientsBase.getData().get(key).getPesel().equalsIgnoreCase(criterionValue)){
                    System.out.println(key + ":" + clientsBase.getData().get(key));
                    check = true;
                }

        }

        else if(criterion.equalsIgnoreCase("adres")){

            for(int key : clientsBase.getData().keySet())
                if(clientsBase.getData().get(key).getPesel().equalsIgnoreCase(criterionValue)) {
                    System.out.println(key + ":" + clientsBase.getData().get(key));
                    check = true;
                }
        }

        else if(criterion.equalsIgnoreCase("id")){

            for(int key : clientsBase.getData().keySet())
                if(clientsBase.getData().get(key).getClientID() == Integer.parseInt(criterionValue)) {
                    System.out.println(key + ":" + clientsBase.getData().get(key));
                    check = true;
                }
        }

        if(!check){

            System.out.println("Nie znaleziono uzytkownika o podanym kryterium");
            System.out.print("Chcesz spróbowac jeszcze raz?\n Podaj [true] aby kontynuuowac: ");
            check = !(input.nextBoolean());
            input.nextLine();
        }

        }while(check != true);
    }


    private static boolean confirmOp(){

        Scanner input = new Scanner(System.in);
        System.out.println("Zatwierdzic operacje?: Y/N");
        String confirm = input.nextLine();

        if(confirm.equalsIgnoreCase("y")) return true;
        else return false;
    }

    private static int findById(){

        int choice;
        System.out.print("Podaj ID: ");
        Scanner input = new Scanner(System.in);
        choice = input.nextInt();
        return choice;
    }
}
