import java.io.Serializable;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Mikołaj on 08.04.2017.
 */
public class Client implements Serializable {

    private int clientID;
    private String firstName;
    private String lastName;
    private String pesel;
    private String adress ;
    private double accountBalance;

    public Client(){

        clientID = 0;
        firstName = null;
        lastName = null;
        pesel = null;
        adress = null;
        accountBalance = 0.0;
    }

    public Client(Map<Integer,Client> clients){

        int i = 0;
        do{
            i++;
            clientID = i;
        }while(clients.containsKey(i));

        System.out.println(clientID);
        int counter = 0 ;
        //Scanner stringWithData = new Scanner(System.in);
        //String allData = stringWithData.nextLine();

        Scanner input = new Scanner("Kamil-Prus-96091106517-lutomiers-960,35");
        input.useDelimiter("-");

        while(input.hasNext()){

            counter++;
            if(counter == 1){
                firstName= input.next();
            }
            else if(counter == 2) {
                lastName = input.next();
            }
            else if(counter == 3){
                pesel = input.next();
            }
            else if(counter == 4){
                adress = input.next();
            }
            else if(counter == 5){
                accountBalance = input.nextDouble();
            }

        }
        System.out.println("Super podales dane!");

    }





    public int getClientID() {
        return clientID;
    }

    public void setClientID(int client) {
        this.clientID = client;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    @Override
    public String toString() {
        return  firstName + " " +
                lastName + " " +
                pesel + " " +
                adress + " " +
                accountBalance
                ;
    }
}
