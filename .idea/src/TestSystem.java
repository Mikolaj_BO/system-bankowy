import java.util.Scanner;

/**
 * Created by Mikołaj on 08.04.2017.
 */
public class TestSystem {

    public static void main(String[] args) {

        String filePath = "Database.bin";
        Database clientsBase = new Database(filePath);

        Scanner input = new Scanner(System.in);

        System.out.println("========================MENU========================");
        System.out.println("[1]-Dodaj uzytkownika\n[2]-Usun uzytkownika.....");

        int choice;


        do {
            choice = input.nextInt();
            switch (choice) {

                case 1:


                    BankOperation.addUser(clientsBase);

                    break;


                case 2:


                    BankOperation.removeUser(clientsBase);

                    break;

                case 3:

                    BankOperation.printAll(clientsBase);
                    break;

                case 4:


                    BankOperation.payIn_Out(clientsBase,"+");

                    break;

                case 5:


                    BankOperation.transfer(clientsBase);

                    break;

                case 6:


                    BankOperation.findByCriterion(clientsBase);

                   break;

                case 7:


                    BankOperation.payIn_Out(clientsBase,"-");

                    break;

                case 8:


                    clientsBase.addDataToFile(filePath);

                    break;
            }


        }while(choice != 8);
    }
}
